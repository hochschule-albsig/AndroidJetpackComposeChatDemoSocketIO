package com.example.mobilesystemeclouddemoapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.mobilesystemeclouddemoapplication.ui.theme.MobileSystemeCloudDemoApplicationTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MobileSystemeCloudDemoApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Main()
                }
            }
        }
    }
}


@Composable
fun Main(chatViewModel: ChatViewModel = viewModel()) {
    val navController = rememberNavController()
    val chatMessages by chatViewModel.chatMessages.observeAsState()
    chatViewModel.registerForUpdates()
    NavHost(navController = navController, startDestination = "register") {
        composable("chat") {
            Chat(chatMessages ?: emptyList()) { chatViewModel.sendMessage(it) }
        }
        composable("register") {
            Register(navController) { chatViewModel.register(it) }
        }
    }
}

@Composable
fun Register(navController: NavController, register: (name: String) -> Unit) {
    var name by remember { mutableStateOf("") }
    Column(modifier = Modifier.padding(8.dp)) {
        TextField(
            value = name,
            onValueChange = { name = it },
            label = { Text("Your name") },
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(onDone = {
                register(name)
                navController.navigate("chat")
            })
        )
        Button(onClick = {
            register(name)
            navController.navigate("chat")
        }) {
            Text("Register")
        }
    }
}

@Composable
fun Chat(chatMessages: List<Message>, sendMessage: (message: String) -> Unit) {
    var message by remember { mutableStateOf("") }

    Column {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .weight(1f)
        ) {
            for (incomingMessage in chatMessages) {
                Row() {
                    Text("${incomingMessage.username.orEmpty()}: ")
                    Text(incomingMessage.message)
                }
                Spacer(modifier = Modifier.height(4.dp))
            }
        }

        Row(modifier = Modifier.padding(4.dp), verticalAlignment = Alignment.CenterVertically) {
            TextField(
                value = message,
                onValueChange = { message = it },
                label = { Text("Your message") },
                modifier = Modifier
                    .weight(1f)
                    .padding(0.dp, 0.dp, 4.dp, 0.dp),
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Send),
                keyboardActions = KeyboardActions(onSend = { ->
                    sendMessage(message)
                })
            )
            Button(onClick = { sendMessage(message) }, modifier = Modifier.wrapContentWidth()) {
                Text("Send")
            }
        }
    }
}

@Preview
@Composable
fun Preview() {
    Surface(color = MaterialTheme.colors.background) {
        Chat(listOf(Message("Hello", "Mario"), Message("Hello World", "Peter"), Message("Hello World", "Christian"))) {}
    }
}