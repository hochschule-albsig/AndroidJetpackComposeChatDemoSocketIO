package com.example.mobilesystemeclouddemoapplication.repository

import android.util.Log
import com.example.mobilesystemeclouddemoapplication.Message
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONException
import org.json.JSONObject


class ChatRepository {

    private val socket: Socket = IO.socket("ws://192.168.2.119:3001").connect()

    suspend fun sendMessage(message: String) {
        Log.e("UI", "Sending message")
        socket.emit("new message", message)
    }

    suspend fun register(name: String) {
        Log.e("UI", "Adding user")
        socket.emit("add user", name)
    }

    suspend fun init(onNewMessage: (message: Message) -> Unit) {

        socket.on(Socket.EVENT_CONNECT) {
            Log.d("UI", "EVENT_CONNECT")
        }.on(Socket.EVENT_CONNECT_ERROR) {
            Log.e("UI", "EVENT_CONNECT_ERROR")
        }.on(Socket.EVENT_DISCONNECT) {
            Log.d("UI", "EVENT_DISCONNECT")
        }.on("new message") {
            val data: String = it[0].toString()
            val jsonObject = JSONObject(data)

            try {
                onNewMessage(
                    Message(
                        jsonObject.getString("message"),
                        jsonObject.getString("username")
                    )
                )
            } catch (e: JSONException) {
                Log.e("UI", "Invalid JSON sent")
                Log.e("UI", e.stackTrace.toString())
            }
        }
    }
}