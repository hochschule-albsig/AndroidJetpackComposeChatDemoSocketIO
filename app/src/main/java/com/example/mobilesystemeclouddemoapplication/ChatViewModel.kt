package com.example.mobilesystemeclouddemoapplication

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mobilesystemeclouddemoapplication.repository.ChatRepository
import kotlinx.coroutines.launch

class ChatViewModel : ViewModel() {
    private val chatRepository = ChatRepository()

    private val _chatMessages = MutableLiveData<List<Message>>()
    val chatMessages: LiveData<List<Message>> = _chatMessages

    private val _name = MutableLiveData<String>()
    val name: LiveData<String> = _name

    fun registerForUpdates() {
        viewModelScope.launch {
            chatRepository.init {
                Log.d("UI", "Got new message $it")
                _chatMessages.postValue(_chatMessages.value.orEmpty() + it)
            }
        }
    }

    fun sendMessage(message: String) {
        viewModelScope.launch {
            chatRepository.sendMessage(message)
        }
        _chatMessages.postValue(_chatMessages.value.orEmpty() + Message(message, name.value))
    }

    fun register(name: String) {
        viewModelScope.launch {
            chatRepository.register(name)
        }
        _name.postValue(name)
    }
}

data class Message(val message: String, val username: String?)

operator fun <T> MutableLiveData<MutableList<T>>.plusAssign(item: T) {
    val value = this.value ?: mutableListOf()
    value.add(item)
    this.value = value
}